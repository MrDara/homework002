package com.example.homework002;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.TextView;

public class DynamicView {

    Context ct;


    public DynamicView() {
    }

    public DynamicView(Context context) {
        this.ct = context;
    }

    @SuppressLint("ResourceAsColor")
    public FrameLayout frame(Context context, String title, String description){
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,150);
        TextView textView1,textView2;
        textView1 = Title(context,title);
        textView2 = Desc(context,description);
        FrameLayout layout = new FrameLayout(context);
        layout.setLayoutParams(params);
        layout.setBackgroundColor(R.color.colorPrimary);
        layout.addView(textView1);
        layout.addView(textView2);
        return layout;
    }


    @SuppressLint("ResourceAsColor")
    public TextView Title(Context context , String title){
        final ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        final TextView textView = new TextView(context);
        textView.setLayoutParams(layoutParams);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(18);
        textView.setTextColor(R.color.colorWrite);
        textView.setText(title);
        textView.setMaxEms(8);
        return textView;
    }

    @SuppressLint("ResourceAsColor")
    public TextView Desc(Context context , String description){
        final ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        final TextView textView = new TextView(context);
        textView.setLayoutParams(layoutParams);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(12);
        textView.setTextColor(R.color.colorWrite);
        textView.setText(description);
        textView.setMaxEms(8);
        return textView;
    }

}
