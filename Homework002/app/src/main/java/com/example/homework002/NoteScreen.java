package com.example.homework002;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.TextView;

public class NoteScreen extends AppCompatActivity {

    private GridLayout rootLayout;
    FrameLayout additem;
    DynamicView dynamicView;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_screen);
        rootLayout = findViewById(R.id.controller_note);
        additem = findViewById(R.id.add_item);

        additem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dynamicView = new DynamicView(context);
                rootLayout.addView(dynamicView.frame(getApplicationContext(),"Hello world","This is java programming"),2);
            }
        });
    }

}